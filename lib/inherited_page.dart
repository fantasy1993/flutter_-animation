import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InheritedPage extends InheritedWidget {
  InheritedPage({Key key, @required this.child, this.data})
      : super(key: key, child: child);

  final Widget child;
  final int data;

  static InheritedPage of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InheritedPage>();
  }

  @override
  bool updateShouldNotify(InheritedPage oldWidget) {
    return oldWidget.data != data;
  }
}
